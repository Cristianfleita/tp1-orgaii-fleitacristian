#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define RED   "\x1B[1;31m"
#define GREEN   "\x1B[32m"
#define YELLOW "\x1B[33m"
#define BLUE   "\x1B[1;34m"
#define MAG   "\x1B[1;35m"
#define CYAN   "\x1B[36m"
#define RESET "\x1B[0m"

float coeficienteA;
float coeficienteB;
float coeficienteC;

void asignarVariables(); 
void comparacion ();
extern void cuadratica(float a, float b, float c);

void asignarVariables()
{
    printf("\nIntroduzca un valor para A:");
    scanf("%f", (float *)&coeficienteA);
    printf(BLUE "\tCoeficiente A guardado" RESET);
    
    printf("\nIntroduzca un valor para B:");
    scanf("%f", (float *)&coeficienteB);
    printf(BLUE "\tCoeficiente B guardado" RESET);
    
    printf("\nIntroduzca un valor para C:");
    scanf("%f", (float *)&coeficienteC);
    printf(BLUE "\tCoeficiente C guardado" RESET);

    printf(MAG "\n\n Los coeficientes ingresados son: %.4f, %.4f, %.4f \n" RESET, coeficienteA, coeficienteB, coeficienteC);
    comparacion();
}

void comparacion ()
{
    char opcion;
    printf(GREEN "\t 1. Presione Y para ejecutar la función con los coeficientes ingresados \n" RESET);
    printf( CYAN"\t 2. Presione R para volver a ingresar los coeficientes \n" RESET);
    printf(RED "\t 3. Presione cualquier otra letra para salir \n" RESET);
    scanf("%s", &opcion);

    if (strcmp(&opcion, "y") == 0 || strcmp(&opcion, "Y") == 0)
    {
      printf(GREEN "\t\tIniciando funcion \n" RESET);  
      cuadratica(coeficienteA, coeficienteB, coeficienteC);
    }
    else if(strcmp(&opcion, "r") == 0 || strcmp(&opcion, "R") == 0) 
    {
        asignarVariables();
    }
    else 
    {
        printf(RED "Saliendo del programa \n" RESET);  
    }
} 

int main()
{    
    char opcion;
    asignarVariables();
}
