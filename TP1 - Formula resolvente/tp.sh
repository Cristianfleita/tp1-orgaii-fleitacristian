#!/bin/bash

#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
    red=`tput setaf 1`;
    green=`tput setaf 2`;
    blue=`tput setaf 4`;
    bg_blue=`tput setab 4`;
    reset=`tput sgr0`;
    bold=`tput setaf bold`;
    yellow=`tput setaf 3`;
    LPURPLE=$(echo -en '\033[01;35m');
    LCYAN=$(echo -en '\033[01;36m');
    MAGENTA=$(echo -en '\033[01;35m');
	LGREEN=$(echo -en '\033[01;32m');
    LBLUE=$(echo -en '\033[01;34m');
	LYELLOW=$(echo -en '\033[01;33m');

#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------
imprimir_menu () 
{
    imprimir_encabezado; 
	echo -e ${LCYAN}"\t\t\t \e[5m TRABAJO PRACTICO 1 ORGA II";
    echo -e "\t\t";
    color "\t\t BIENVENIDO \e[0m";
    echo -e ${LPURPLE}"\t\t\t a.  Compilar archivo .asm";
    echo -e ${LBLUE}"\t\t\t b.  Compilar archivo .c ";
    echo -e ${LGREEN}"\t\t\t c.  Compilar archivos y ejecutar ";
    echo -e ${LYELLOW}"\t\t\t d.  Ejecutar ";
    echo -e ${LCYAN}"\t\t\t q.  \e[5m Salir\e[0m";
    echo "";
    echo -e ${LGREEN}"\t\t\t Escriba una opción y presione ENTER \e[0m";
}


imprimir_encabezado () 
{
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e ${LPURPLE}"`date +"%d-%m-%Y %T" `\t\t\t\t\t ORGA II";
    #Se agregan colores a encabezado
    echo -e "\t\t ${bold}${yellow}$1\t\t${reset}";
}

color()
{
	echo -e "\t\t ${bold}${red}$1\t\t${reset}";
}

esperar () 
{
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}
#------------------------------------------------------
# FUNCIONES del MENU
#------------------------------------------------------
a_funcion () 
{    
	sudo nasm -f elf32 cuadratica.asm -o cuadratica.o;
	echo -e ${LPURPLE}"\t\t\tArchivo .asm compilado \e[0m"
}

b_funcion () 
{
	sudo gcc -m32 -o main  cuadratica.o resolvente.c
	echo -e ${LPURPLE}"\t\t\tArchivo .c compilado \e[0m"
}

c_funcion () 
{
	a_funcion;
	b_funcion;
	d_funcion;
}

d_funcion()
{
	echo -e ${LPURPLE}"\t\t\tIniciando ejecución del programa \e[0m"
	./main
}
malaEleccion () 
{
    echo -e ${LPURPLE}" \t\t\tValor invalido, debe ingresar a,b,c,d o q \e[0m" ;
}

#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------
while  true
do
	# 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;
 	case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        q|Q) break;;
        *) malaEleccion;;
    esac
    esperar;
done

