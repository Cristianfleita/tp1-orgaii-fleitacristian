# Resolución TP I - Resolvente en ASM 🚀

## Pre-requisitos 📋

_Antes de ejecutar el archivo **tp.sh** hay que darle permisos:_

```
chmod 700 tp.sh
```

## Menú inicial 📌
Al ejecutar el archivo bash **tp.sh** aparecerá lo siguiente en la consola:

![](https://gitlab.com/Cristianfleita/tp1-orgaii-fleitacristian/-/raw/72f7cdd8995ede6547bbce981925efb6a8145eb2/screenshots/Captura_de_pantalla_de_2020-11-02_17-20-29.png "Menú")

### Opciones del menú 📢
* La **Opción a** compila el archivo .asm
* La **Opción b** compila el archivo .c
* La **Opción c** compila el archivo .asm, el archivo .c y los ejecutar
* La **Opción d** solo ejecuta *(esta opción se debe seleccionar sólo si antes ya se compilo el programa)*

## Ejecución 🛠️
Cuando se seleccionar ejecutar el programa el archivo bash va a ejecutar el archivo **main.o** el cual al iniciar nos va a pedir 3 valores para los coeficientes *(internamente se utiliza la función scanf para obtener los parámetros ingresados por consola)*. Notemos que luego de ingresar los coeficientes se mostrará un mensaje indicando que se guardó satisfactoriamente.
![](https://gitlab.com/Cristianfleita/tp1-orgaii-fleitacristian/-/raw/master/screenshots/Captura_de_pantalla_de_2020-11-02_17-35-46.png "coeficientes")

### Luego el programa nos muestra que valores ingresamos y nos deja seleccionar tres opciones 📢:
* **Opción Y** para confirmar y continuar la ejecución llamando a la función de nasm
* **Opción R** para volver a ingresar los coeficientes
* **Cualquier tecla** para salir del programa
![](https://gitlab.com/Cristianfleita/tp1-orgaii-fleitacristian/-/raw/master/screenshots/Captura_de_pantalla_de_2020-11-02_17-45-14.png "opciones")

### Resultado 📄
Luego de ingresar la **opción Y** el programa llamará a la función externa en nasm la cual se encargará de emular la función cuadrática y va a imprimir los resultados en la consola.
![](https://gitlab.com/Cristianfleita/tp1-orgaii-fleitacristian/-/raw/master/screenshots/Captura_de_pantalla_de_2020-11-02_18-02-11.png "resultado")


