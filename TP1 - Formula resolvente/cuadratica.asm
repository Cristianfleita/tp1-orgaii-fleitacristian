;%include "io.inc"
extern printf

section .data
    formato db "Los coeficientes obtenidos son: %f, %f, %f",10,0
    coeficienteA dq 0.0
    coeficienteB dq 0.0
    coeficienteC dq 0.0
    resParcial dq 0.0
    print db " x = %f", 10,0
    cuatro dq 4.0
    raizPositiva dq 0.0
    raizNegativa dq 0.0

section .text
    global cuadratica

cuadratica:
    ;write your code here
    xor eax, eax
    enter 0,0
    fld  dword[ebp + 8]
    fld  dword[ebp + 12]
    fld  dword[ebp + 16]
    fstp qword [coeficienteC]
    fstp qword [coeficienteB]
    fstp qword [coeficienteA]
    
    call calcIntermedio
    call ramaPositiva
    call ramaNegativa
    jmp terminar
         
    calcIntermedio:
        fld qword[coeficienteA]
        fld qword[coeficienteC]   
        fmul; a*c
        fld qword [cuatro]
        fmul ; 4*a*c
    
        fld  qword[coeficienteB]
        fmul st0, st0 ; b^2
 
        fsub st0, st1 ; (b^2) - (4 *a *c)
        fsqrt ; Hacemos la raiz cuadrada 

        fstp qword [resParcial] ; obtenemos el resultado parcial
    ret
    
    ramaPositiva:
        fld qword[coeficienteB]
        fchs; -b        
        fld qword[resParcial]
        fadd ; -b + raiz( (b^2) - 4*a*c)                

        fld qword[coeficienteA]        
        fadd st0, st0 ; 2*a        
        fdiv ;-b + raiz( (b^2) - 4*a*c)  / (2*a)
        
        fstp qword [raizPositiva]; sacamos el valor de la pila
        push dword [raizPositiva + 4]
        push dword [raizPositiva]
        push print        
        call printf
        add esp, 12
    ret    

    ramaNegativa:
        fld qword[coeficienteB]
        fchs; -b
        fld qword[resParcial]
        fsub; -b - raiz( (b^2) - 4*a*c)        
        
        fld qword[coeficienteA]
        fadd st0, st0 ; 2*a
        fdiv ;-b - raiz( (b^2) - 4*a*c)  / (2*a)
        
        fstp qword [raizNegativa] ; sacamos el valor de la pila
        push dword [raizNegativa + 4]
        push dword [raizNegativa]
        push print        
        call printf
        add esp, 12
       
    ret    
  
    terminar:
        leave
    ret