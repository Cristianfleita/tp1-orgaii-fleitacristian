%include "io.inc"
extern printf

section .data
    formato db "El resultado de la suma es: %.2f",10,0
    res dq 0.0
    array dd 12.4, 20.9, 20.1, 30.9
    cantElementos db 4

section .text
    global CMAIN

CMAIN:
    mov ebp, esp; for correct debugging

    ;Limpiamos los registros a utilizar
    xor eax, eax
    xor ebx, ebx
    xor esi, esi    
    
    mov al, 4
    mov cl, [cantElementos]
    mul cl; guardamos en al  cantElementos * peso de cada elemento 
    sub al, 4 ; le restamos 4 porque el primer elemento ya lo obtenemos
    
    ;Guardamos la direccion de memoria del vector para poder recorrerlo
    mov ebx,array
    
    ; Apilamps el primer elemento del vector en la pila de la FPU
    FLD dword [ebx]
    
    CICLO: ; Verificamos si podemos seguir recorriendo el vector o no
        add esi, 4
        cmp esi, eax
        JLE APILARYSUMAR
        JG TERMINAR
    
    APILARYSUMAR: ;Apilamos y sumamos los valores 
        FLD dword [ebx + esi]
        FADD
        jmp CICLO
         
    TERMINAR:
       ; Luego de sumar todos los valores del vector
       FST qword[res] ;Guardamos la suma final en esta variable
       
       ; Luego la guardamos en la pila para imprimirla
       push dword[res + 4] 
       push dword[res]
       push formato 
       call printf

       add esp, 12                 
    ret