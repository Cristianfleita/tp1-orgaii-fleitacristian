# Resolución de los ejercicios para el TP1 de Organización del computador II 📖

1. En la carpeta [**Ejercicios obligatorios**](https://gitlab.com/Cristianfleita/tp1-orgaii-fleitacristian/-/tree/master/Ejercicios%20obligatorios) se encuentran la resolución de los ejercicos obligatorios de la práctica de gestión de memoria y FPU.
2. En la carpeta [**TP1-Formula resolvente**](https://gitlab.com/Cristianfleita/tp1-orgaii-fleitacristian/-/tree/master/TP1%20-%20Formula%20resolvente) se encuentra un programa para la arquitectura IA32 que calcula las raíces de una función cuadrática a través de la fórmula resolvente.


